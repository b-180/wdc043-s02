import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int index = -1;

        Scanner scan = new Scanner(System.in).useDelimiter("\\n");

        String[] fruitsArr = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock:" + Arrays.toString(fruitsArr));
        System.out.println("Which fruit would you like to get the index of?");
        String fruitIndex = scan.nextLine();
        System.out.println("The index of " + fruitIndex + " is: " + Arrays.asList(fruitsArr).indexOf(fruitIndex));

        System.out.println();
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);

        System.out.println();
        HashMap<String, Integer> inventory = new HashMap<String, Integer>()
        {{
            put("toothpaste", 15);
            put("toothbrush", 20);
            put("soap", 12);
        }};

        System.out.println("Our current inventory consists of: ");
        System.out.println(inventory);



    }
}
